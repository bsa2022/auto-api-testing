import AuthorSettingsModel from "../models/author/author-settings.model";
import AbstractService from "./abstract.service";

export class AuthorService extends AbstractService {
    public constructor() {
        super("/author");
    }

    public getSettings() {
        return this.httpHelper.get("");
    }

    public setSettings(data: AuthorSettingsModel) {
        return this.httpHelper.post("", data);
    }

    public getOverview(id: string) {
        return this.httpHelper.get(`/overview/${id}`);
    }
}
