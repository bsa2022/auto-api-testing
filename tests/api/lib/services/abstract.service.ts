import HttpHelper from "../helpers/http.helper";

export default abstract class AbstractService {
    protected readonly httpHelper: HttpHelper;

    public constructor(basePath: string) {
        this.httpHelper = new HttpHelper(basePath);
    }
}
