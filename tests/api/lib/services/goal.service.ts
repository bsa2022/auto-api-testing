import AbstractService from "./abstract.service";

export class GoalService extends AbstractService {
    public constructor() {
        super("/goals");
    }

    public getAll() {
        return this.httpHelper.get("");
    }
}
