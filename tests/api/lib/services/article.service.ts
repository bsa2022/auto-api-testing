import CreateArticleModel from "../models/articles/create-article.model";
import AbstractService from "./abstract.service";

export class ArticleService extends AbstractService {
    public constructor() {
        super("/article");
    }

    public createArticle(data: CreateArticleModel) {
        return this.httpHelper.post("", data);
    }

    public getMy() {
        return this.httpHelper.get("/author");
    }

    public getArticle(id: string) {
        return this.httpHelper.get(`/${id}`);
    }
}
