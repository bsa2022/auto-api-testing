import axios, { Axios } from "axios";
import ApiConstants from "../constants/api.constants";
import SessionCollection from "./session-collection.helper";
import Session from "./session.helper";

type Headers = Record<string, string | string[]>;

interface ApiSession {
    authtoken?: string;
}

export default class HttpHelper extends Axios {
    protected static readonly sessions = new SessionCollection<ApiSession>();

    public constructor(basePath?: string) {
        super({
            ...axios.defaults,
            baseURL: ApiConstants.BaseUrl + (basePath ?? ""),
            headers: axios.defaults.headers.common,
            validateStatus: () => true,
        });

        this.interceptors.request.use((config) => ({
            ...config,
            headers: {
                ...this.getAuthorizationHeader(),
            },
            startTime: performance.now(),
        }));

        this.interceptors.response.use((response) => {
            const endTime = performance.now();

            return {
                ...response,
                duration: endTime - (response.config.startTime ?? endTime),
            };
        });
    }

    public static addSession(id?: string | number) {
        return this.sessions.add(new Session<ApiSession>({}), id);
    }

    public static switchSession(id: string | number) {
        this.sessions.switch(id);
    }

    public static addAndEnterSession(id?: string | number) {
        const addedId = this.addSession(id);
        this.switchSession(addedId);
    }

    public static removeSession(id?: string | number) {
        this.sessions.remove(id);
    }

    public static exitSession() {
        return this.sessions.exit();
    }

    public static exitAndRemoveSession() {
        const id = this.exitSession();
        this.removeSession(id);
    }

    public static setAuthtoken(value: string) {
        if (!this.sessions.session) {
            throw new Error("Must be in session to set authtoken");
        }

        this.sessions.session.set("authtoken", value);
    }

    protected getAuthorizationHeader(): Headers {
        const token = this.getAuthtoken();

        if (token) {
            return { Authorization: `Bearer ${token}` };
        }

        return {};
    }

    protected getAuthtoken() {
        return HttpHelper.sessions.session?.get("authtoken");
    }
}
