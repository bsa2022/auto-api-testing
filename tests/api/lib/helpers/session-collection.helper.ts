import Session from "./session.helper";

export default class SessionCollection<T extends object> {
    protected readonly sessions: Record<string | number, Session<T>> = {};
    private currentSessionId?: string | number;
    private currentSession?: Session<T>;

    public get session() {
        return this.currentSession;
    }

    public switch(id: string | number) {
        const newSession = this.sessions[id];

        if (!newSession) {
            throw new Error("Session is not found");
        }

        this.currentSessionId = id;
        this.currentSession = newSession;
    }

    public exit() {
        const oldId = this.currentSessionId;

        this.currentSessionId = undefined;
        this.currentSession = undefined;

        return oldId;
    }

    public add(session: Session<T>): string | number;
    public add(session: Session<T>, id?: string | number): string | number;
    public add(session: Session<T>, id?: string | number | Session<T>) {
        if (typeof id === "string" || typeof id === "number") {
            this._add(session, id);
        } else {
            this._add(session, 0);
        }

        return id ?? 0;
    }

    public remove(id: string | number = 0) {
        const session = this.sessions[id];

        if (!session) {
            throw new Error("Session is not found");
        }

        if (this.currentSessionId && this.currentSessionId === id) {
            throw new Error("Can't remove current session");
        }

        delete this.sessions[id];
    }

    protected _add(session: Session<T>, id: string | number) {
        if (this.sessions[id]) {
            throw new Error("Session already exists");
        }

        this.sessions[id] = session;
    }
}
