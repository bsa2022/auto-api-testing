export default interface MeInfoModel {
    id: string;
    avatar: string | null;
    email: string;
    emailVerified: boolean;
    nickname: string | null;
    role: {
        name: "AUTHOR" | "USER";
    };
}
