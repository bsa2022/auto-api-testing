export default interface ShortUserInfo {
    id: string;
    username: string | null;
    email: string;
    role: "AUTHOR" | "USER";
}
