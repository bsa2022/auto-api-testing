export default interface AuthorSettingsModel {
    id: string;
    avatar: string | null;
    firstName: string;
    lastName: string;
    company: string;
    job: string;
    website: string;
    twitter: string;
    location: string;
    biography: string;
}
