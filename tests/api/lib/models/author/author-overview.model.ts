import ArticleModel from "../articles/article.model";
import AuthorCourseModel from "../courses/author-course.model";

export default interface AuthorOverviewModel {
    id: string;
    userId: string;
    avatar: string | null;
    firstName: string;
    lastName: string;
    biography: string;
    numberOfSubscribers: number;
    printFollowButton: boolean;
    schoolId: string;
    schoolName: string;
    articles: ArticleModel[];
    courses: AuthorCourseModel[];
}
