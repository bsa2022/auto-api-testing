export default interface CreateArticleModel {
    name: string;
    text: string;
    image: string;
}
