import { expect } from "chai";
import HttpHelper from "api/lib/helpers/http.helper";
import ArrayHelper from "api/lib/helpers/array.helper";
import IDHelper from "api/lib/helpers/id.helper";
import ShortCourseModel from "api/lib/models/courses/short-course.model";
import MeInfoModel from "api/lib/models/user/me-info.model";
import GoalModel from "api/lib/models/goal/goal.model";

import {
    AuthService,
    CourseService,
    UserService,
    CourseCommentService,
    GoalService,
    StudentService,
} from "api/lib/services";

import { courseCommentMock } from "../data/mocks";

import {
    allCoursesSchema,
    courseCommentSchema,
    courseCommentsSchema,
    goalSchema,
    goalsSchema,
    loginResponseSchema,
    meInfoSchema,
} from "../data/schemas";

import {
    invalidEmailLoginData,
    invalidPasswordLoginData,
    loginData,
} from "../data/student-chain/static";

import {
    courseCommentExpected,
    meInfoExpected,
    goalExpected,
} from "../data/student-chain/expected";

const authService = new AuthService();
const userService = new UserService();
const courseService = new CourseService();
const courseCommentService = new CourseCommentService();
const goalService = new GoalService();
const studentService = new StudentService();

describe("Student chain", () => {
    let meInfo: MeInfoModel;
    let courseId: string;
    let unexistingCourseId: string;
    let course: ShortCourseModel;
    let commentId: string;
    let commentText: string;
    let commentDate: string;
    let goal: GoalModel;

    before(() => HttpHelper.addAndEnterSession("default"));
    after(() => HttpHelper.exitAndRemoveSession());

    it("should reject log in with invalid email", async () => {
        const response = await authService.login(invalidEmailLoginData);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(401);
    });

    it("should reject log in with invalid password", async () => {
        const response = await authService.login(invalidPasswordLoginData);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(401);
    });

    it("should log in successfully with valid credentials", async () => {
        const response = await authService.login(loginData);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(loginResponseSchema);

        HttpHelper.setAuthtoken(response.data.accessToken);
    });

    it("should get correct current user info", async () => {
        const response = await userService.getMe();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(meInfoSchema);
        expect(response.data).to.deep.include(meInfoExpected({ loginData }));

        meInfo = response.data;
    });

    it("should get all courses", async () => {
        const response = await courseService.getAllCourses();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(allCoursesSchema);

        const courseIds = (response.data as ShortCourseModel[]).map(
            ({ id }) => id
        );

        course = ArrayHelper.choose(response.data);
        courseId = course.id;
        unexistingCourseId = IDHelper.getUnexistingId(courseIds);
    });

    it("should reject to create comment for unexisting course", async () => {
        const commentData = courseCommentMock(unexistingCourseId);
        const response = await courseCommentService.createComment(commentData);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(404);
    });

    it("should create comment for course", async () => {
        const commentData = courseCommentMock(course.id);
        commentText = commentData.text;

        const response = await courseCommentService.createComment(commentData);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(courseCommentSchema);

        expect(response.data).to.deep.include(
            courseCommentExpected({
                courseId,
                commentText,
                meInfo,
            })
        );

        commentId = response.data.id;
        commentDate = response.data.createdAt;
    });

    it("should get comments for course", async () => {
        const response = await courseCommentService.getForCourse(courseId);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(courseCommentsSchema);

        expect(response.data).to.deep.include(
            courseCommentExpected({
                commentId,
                courseId,
                commentText,
                commentDate,
                meInfo,
            })
        );
    });

    it("should get weekly goals", async () => {
        const response = await goalService.getAll();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(goalsSchema);

        goal = ArrayHelper.choose(response.data);
    });

    it("should reject to set weekly goal for unauthorized user", async () => {
        HttpHelper.addAndEnterSession("unauthorized");

        const response = await studentService.setGoal(goal.id);

        HttpHelper.switchSession("default");
        HttpHelper.removeSession("unauthorized");

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(401);
    });

    it("should set weekly goal", async () => {
        const response = await studentService.setGoal(goal.id);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
    });

    it("should get correct weekly goal", async () => {
        const response = await studentService.getGoal();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(goalSchema);
        expect(response.data).to.deep.include(goalExpected({ goal }));
    });
});
