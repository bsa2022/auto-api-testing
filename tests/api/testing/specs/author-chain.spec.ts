import { expect } from "chai";
import HttpHelper from "api/lib/helpers/http.helper";
import AuthorSettingsModel from "api/lib/models/author/author-settings.model";
import CreateArticleModel from "api/lib/models/articles/create-article.model";
import ArticleWithAuthorModel from "api/lib/models/articles/article-with-author.model";
import MeInfoModel from "api/lib/models/user/me-info.model";

import {
    AuthService,
    AuthorService,
    UserService,
    ArticleService,
    ArticleCommentService,
} from "api/lib/services";

import {
    authorSettingsMock,
    articleMock,
    articleCommentMock,
} from "../data/mocks";

import {
    loginResponseSchema,
    authorSettingsSchema,
    meInfoSchema,
    overviewSchema,
    articleWithAuthorSchema,
    articlesSchema,
    fullArticleSchema,
    articleCommentSchema,
    articleCommentsSchema,
} from "../data/schemas";

import { invalidLoginData, loginData } from "../data/author-chain/static";

import {
    setSettingsExpected,
    meInfoExpected,
    overviewExpected,
    articleWithAuthorExpected,
    fullArticleExpected,
    articleCommentExpected,
} from "../data/author-chain/expected";

const authService = new AuthService();
const authorService = new AuthorService();
const userService = new UserService();
const articleService = new ArticleService();
const articleCommentService = new ArticleCommentService();

describe("Author chain", () => {
    let userId: string;
    let meInfo: MeInfoModel;
    let authorId: string;
    let authorSettings: AuthorSettingsModel;
    let articleId: string;
    let articleData: CreateArticleModel;
    let articleList: ArticleWithAuthorModel[];
    let commentId: string;
    let commentText: string;
    let commentDate: string;

    before(() => HttpHelper.addAndEnterSession());
    after(() => HttpHelper.exitAndRemoveSession());

    it("should reject log in with invalid password", async () => {
        const response = await authService.login(invalidLoginData);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(401);
    });

    it("should log in successfully with valid credentials", async () => {
        const response = await authService.login(loginData);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(loginResponseSchema);

        HttpHelper.setAuthtoken(response.data.accessToken);
    });

    it("should successfully get author settings", async () => {
        const response = await authorService.getSettings();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(authorSettingsSchema);

        authorId = response.data.id;
    });

    it("should successfully set author settings", async () => {
        authorSettings = authorSettingsMock(authorId);
        const response = await authorService.setSettings(authorSettings);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.be.deep.equal(setSettingsExpected());
    });

    it("should get correct current user info", async () => {
        const response = await userService.getMe();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(meInfoSchema);

        expect(response.data).to.deep.include(
            meInfoExpected({ loginData, authorSettings })
        );

        userId = response.data.id;
        meInfo = response.data;
    });

    it("should get correct author overview", async () => {
        const response = await authorService.getOverview(authorId);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(overviewSchema);

        expect(response.data).to.include(
            overviewExpected({ userId, authorId, authorSettings })
        );
    });

    it("should create article", async () => {
        articleData = articleMock();
        const response = await articleService.createArticle(articleData);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(articleWithAuthorSchema);

        expect(response.data).to.deep.include(
            articleWithAuthorExpected({ articleData, authorSettings })
        );

        articleId = response.data.id;
    });

    it("should get list of articles with created article", async () => {
        const response = await articleService.getMy();

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(articlesSchema);

        expect(response.data).to.deep.include(
            articleWithAuthorExpected({
                articleId,
                articleData,
                authorSettings,
            })
        );

        articleList = response.data;
    });

    it("should get created article", async () => {
        const response = await articleService.getArticle(articleId);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(fullArticleSchema);

        expect(response.data).to.deep.include(
            fullArticleExpected({
                articleId,
                articleData,
                userId,
                authorId,
                authorSettings,
                articleList,
            })
        );
    });

    it("should create comment for article", async () => {
        const commentData = articleCommentMock(articleId);
        commentText = commentData.text;

        const response = await articleCommentService.createComment(commentData);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(articleCommentSchema);

        expect(response.data).to.deep.include(
            articleCommentExpected({
                articleId,
                commentText,
                meInfo,
            })
        );

        commentId = response.data.id;
        commentDate = response.data.createdAt;
    });

    it("should get comments for articles", async () => {
        const response = await articleCommentService.getForArticle(articleId);

        expect(response).to.have.normalExecutionTime;
        expect(response).to.have.status(200);
        expect(response.data).to.have.jsonSchema(articleCommentsSchema);

        expect(response.data).to.deep.include(
            articleCommentExpected({
                commentId,
                articleId,
                commentText,
                commentDate,
                meInfo,
            })
        );
    });
});
