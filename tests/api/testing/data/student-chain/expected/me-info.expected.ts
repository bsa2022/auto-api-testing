import LoginModel from "api/lib/models/auth/login.model";
import MeInfoModel from "api/lib/models/user/me-info.model";

interface Options {
    loginData: LoginModel;
}

export function meInfoExpected({ loginData }: Options): Partial<MeInfoModel> {
    return {
        email: loginData.email,
        emailVerified: true,
        role: {
            name: "USER",
        },
    };
}
