import invalidLoginData from "./invalid-login.data.json";
import loginData from "./login.data.json";

export { invalidLoginData, loginData };
