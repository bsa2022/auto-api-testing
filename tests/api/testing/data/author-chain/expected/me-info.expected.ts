import LoginModel from "api/lib/models/auth/login.model";
import AuthorSettingsModel from "api/lib/models/author/author-settings.model";
import MeInfoModel from "api/lib/models/user/me-info.model";

interface Options {
    loginData: LoginModel;
    authorSettings: AuthorSettingsModel;
}

export function meInfoExpected({
    loginData: { email },
    authorSettings: { avatar },
}: Options): Partial<MeInfoModel> {
    return {
        avatar,
        email,
        emailVerified: true,
        role: {
            name: "AUTHOR",
        },
    };
}
