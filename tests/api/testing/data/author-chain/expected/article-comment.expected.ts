import MeInfoModel from "api/lib/models/user/me-info.model";
import ArticleCommentModel from "api/lib/models/article-comment/article-comment.model";

interface Options {
    commentId?: string;
    commentDate?: string;
    commentText: string;
    articleId: string;
    meInfo: MeInfoModel;
}

export function articleCommentExpected({
    commentId: id,
    articleId,
    commentText: text,
    commentDate: date,
    meInfo: { id: userId, email, nickname: username },
}: Options): Partial<ArticleCommentModel> {
    return {
        ...(id ? { id } : {}),
        ...(date ? { createdAt: date, updatedAt: date } : {}),
        articleId,
        sourceId: articleId,
        text,
        user: {
            id: userId,
            email,
            username,
            role: "AUTHOR",
        },
    };
}
