import "chai";
import "chai-json-schema";

declare global {
    namespace Chai {
        interface Assertion {
            normalExecutionTime: Assertion;
            status(status: number): Assertion;
        }

        interface LanguageChain {
            normalExecutionTime: Assertion;
            status(status: number): Assertion;
        }
    }
}
