import chai from "chai";
import chaiJsonSchema from "chai-json-schema";
import chaiAxios from "plugins/axios";

chai.use(chaiJsonSchema);
chai.use(chaiAxios);
