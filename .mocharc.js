const path = require("path");

module.exports = {
    require: [
        "ts-node/register",
        "tsconfig-paths/register",
        "source-map-support/register",
        path.join(__dirname, "tests", "setup.ts"),
    ],
    timeout: 100000,
    bail: true,
    parallel: false,
};
