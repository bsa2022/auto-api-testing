# Auto API Testing

## Notes:

-   All requests have max execution time of 5s. If your internet is slow, they can fail
-   I used a little bit different architecture than was offered in the lecture

## Preconditions:

### Author chain:

-   Author with known credentials exists

### Student chain:

-   Student with known credentials exists
-   At least one course exists
-   At least one daily goal exists

## Setup:

-   Find all files ending with `.json.example` located in `./tests/**/testing/data/**/static`
-   Create new files without the `.example` suffix in the same directories
-   Copy contents from corresponding example files
-   Replace the placeholders (enclosed in `<` and `>`) with your data
-   `npm install`

## Test:

-   `npm test`
